# Instalador de sentrifugo.

Descarga, instala y configura el software de Sentrifugo en un servidor.

Para la instalación, descarga e instala los siguientes programas en el sevidor.

* apache2
* mariadb-server
* php
* libapache2-mod-php
* php-common
* php-mbstring
* php-xmlrpc
* php-soap
* php-gd
* php-xml
* php-intl
* php-mysql
* php-cli
* php
* php-ldap
* php-zip
* php-curl
* unzip
* wget

Luego inicia y habilita apache y mariadb. Posteriormente crea el usuario sentrifugo en la base de datos.

### Modo de uso.

Primero se deben de configurar las variables

`<server admin mail>`

`<servername>`

`<serveralias>`

`<password>`

que se encuentran dentro del script. Después simplemente ejecutar con

```shell
./installer.sh [--skip-dependencies]

```

El parámetro *--skip-dependencies* simplemente funciona si no se desean reinstalar los paquetes previamente instalados con apt.

### **Nota**

En lo que no está el sistema que interactúa con mysql_secure_installation,
dejar la contraseña de root vacía y contestar a la pregunta sobre ponerla
con 'N', después de eso contestar afirmativamente a todas las preguntas.
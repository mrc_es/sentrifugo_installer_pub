#!/bin/bash

: << __docstring__

	Programa: installer.sh
	Autor: Marco Cuauhtli Elizalde Sánchez

		Descarga e instala sentrifugo, con sus respectivas dependencias
	como apache2, mariadb, apache y luego modifica su configuración
	para poder ejecutarse.

	Nota: En lo que no está el sistema que interactúa con mysql_secure_installation
		dejar la contraseña de root vacía y contestar a la pregunta sobre ponerla
		con 'N', después de eso contestar afirmativamente a todas las preguntas.

__docstring__

export _progname="$( basename "$( readlink -f "$0" )" )"
export _dirname="$( dirname "$( readlink -f "$0" )" )"

#[START] Server settings
declare -r _serveradmin='<server admin mail>'
declare -r _documentroot='/var/www/html'
declare -r _servername='<servername>'
declare -r _serveralias='<serveralias>'
#[END] Server settings

#[START] Database settings
declare -r _dbname='sentrifugodb'
declare -r _dbhost='localhost'

declare -A _dbuserinfo
_dbuserinfo=(
	[name]='sentrifugo'
	[pass]='<password>'
)
#[END] Database settings

#[START] Status messages
declare -r E_NON_SUDO_USER='Se debe de correr como superusuario'
#[END] Status messages

#[START] Status codes
declare -r E_NON_SUDO_USER_STATUS_CODE=1
#[END] Status codes

export E_NON_SUDO_USER
export _serveradmin
export _documentroot
export _servername
export _serveralias

check_permisions() {

	[[ "$(id -un)" != root ]] \
		&& echo "$E_NON_SUDO_USER" >&2 \
		&& exit $E_NON_SUDO_USER_STATUS_CODE
}

install_dependencies() {

	declare -a dependencies
	dependencies=(
		apache2
		mariadb-server
		php
		libapache2-mod-php
		php-common
		php-mbstring
		php-xmlrpc
		php-soap
		php-gd
		php-xml
		php-intl
		php-mysql
		php-cli
		php
		php-ldap
		php-zip
		php-curl
		unzip
		wget
	)

	apt-get upgrade -y 
	apt-get update -y 
	apt install -y ${dependencies[@]}
}

enable_servers() {

	declare -a services=(
		apache2	
		mariadb
	)

	declare -a acciones=(
		start
		enable
	)

	for servicio in ${services[@]}
	do
		xargs -I % systemctl % $servicio  <<< "$(IFS=$'\n'; echo -e "${acciones[*]}")"
	done
}

create_sentrifugo_db() {

	sudo mysql_secure_installation

	declare -a consultas
	consultas=(
		"CREATE DATABASE ${_dbname}"
		"CREATE USER '${_dbuserinfo[name]}'@'${_dbhost}' IDENTIFIED BY '${_dbuserinfo[pass]}'"
		"GRANT ALL ON ${_dbname}.* TO '${_dbuserinfo[name]}'@'${_dbhost}' IDENTIFIED BY '${_dbuserinfo[pass]}' WITH GRANT OPTION"
		"FLUSH PRIVILEGES"		
	)

	declare complete_query="$(IFS=';'; echo "${consultas[*]}"';')"

	mysql -u root -p"" -e "${complete_query}"
}

download_and_install_sentrifugo() {

	wget 'http://www.sentrifugo.com/home/downloadfile?file_name=Sentrifugo.zip' -O '/tmp/Sentrifugo.zip'
	unzip /tmp/Sentrifugo.zip -d /tmp/

	cp -r /tmp/Sentrifugo_3.2/* $_documentroot
	chown -R www-data:www-data $_documentroot
	chmod -R 755 $_documentroot

	sed -ri 's/^phpSettings\.error_reporting = E_All$/phpSettings\.error_reporting = E_ALL \& \~E_DEPRECATED \& \~E_STRICT/g' $_documentroot/application/configs/application.ini
	echo "<VirtualHost *:80>
	ServerAdmin ${_serveradmin}
	DocumentRoot ${_documentroot}
	ServerName ${_servername}
	ServerAlias ${_serveralias}
	
	<Directory $_documentroot/>
		Options +FollowSymlinks
		AllowOverride All
		Require all granted
	</Directory>
	
	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined

</VirtualHost>" > /etc/apache2/sites-available/sentrifugo.conf
	a2ensite sentrifugo
	a2enmod rewrite
	systemctl restart apache2

	rm -f $_documentroot/index.html
}

get_parameters() {

	while [[ $# > 0 ]]
	do
		case "$1" in
			--skip-dependencies )
				shift
				export skip_dependencies=0
				;;
			* )
				shift
				;;
		esac
	done
}

main() {

 	check_permisions	
	get_parameters $@
	[[ "${skip_dependencies+x}" ]] || install_dependencies
	enable_servers
	create_sentrifugo_db
	download_and_install_sentrifugo
}

main $@
